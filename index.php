<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\AreaCalculator\Rectangle;
$rectangle1 = new Rectangle();
var_dump($rectangle1);

use Pondit\Calculator\AreaCalculator\Square;
$square1 = new Square();
var_dump($square1);

use Pondit\Calculator\AreaCalculator\Triangle;
$triangle1 = new Triangle();
var_dump($triangle1);

use Pondit\Calculator\AreaCalculator\Circle;
$circle = new Circle();
var_dump($circle);