<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitfc00a2dba6659e659c7a93e46c856471
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Pondit\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Pondit\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitfc00a2dba6659e659c7a93e46c856471::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitfc00a2dba6659e659c7a93e46c856471::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
